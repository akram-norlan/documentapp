import { Template } from "meteor/templating";
import { ReactiveVar } from "meteor/reactive-var";
import { Records } from "../import/api/Records.js";

import "./main.html";
import "./dashboard.html";
import "./new.html";
import "./y.html";

Router.route("/", {
  template: "dashboard"
});

Router.route("/dashboard", {
  template: "dashboard"
});

Router.route("new", {
  template: "new"
});

Router.route("y/:_year", {
  data: function() {},
  template: "y"
});

// new
Template.new.events({
  "submit form": function(event) {
    event.preventDefault();

    var active = $("#frm_active").val();
    var active_date = new Date($("#frm_active_date").val()).toISOString() ;
    var expiry_date = new Date($("#frm_expiry_date").val()).toISOString() ;

    Records.insert({
      active: active,
      active_date: active_date,
      expiry_date: expiry_date
    });
    $("#frm_active").val('')
    $("#frm_active_date").val('')
    $("#frm_expiry_date").val('')
  }
});

// y
Template.y.helpers({
  list: function() {
    return Records.find({
      active_date: {
        $gte: Router.current().params._year + "-01-00T00:00:00.000Z",
        $lte: Router.current().params._year + "-12-31T23:59:59.000Z"
      }
    });
  },
  year:function(){
    return Router.current().params._year;
  }
});

// dashboard
Template.dashboard.activedocuments = function() {
  return {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false
    },
    title: {
      text: "Number of Active and Inactive Documents"
    },
    tooltip: {
      pointFormat: "<b>{point.percentage:.1f}%</b>"
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: "pointer",
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.percentage:.1f} %',
          style: {
            color:
              (Highcharts.theme && Highcharts.theme.contrastTextColor) ||
              "black"
          },
          connectorColor: "silver"
        }
      }
    },
    series: [
      {
        type: "pie",
        name: "genre",
        data: [
          [
            "Active",
            Records.find({
              active: "y"
            }).count()
          ],
          [
            "Inactive",
            Records.find({
              active: "n"
            }).count()
          ]
        ]
      }
    ]
  };
};

Template.dashboard.documentsbyyear = function() {
  return {
    chart: {
      type: "column"
    },
    title: {
      text: "Number of Documents By Year"
    },
    subtitle: {
      text: ""
    },
    xAxis: {
      categories: ["2015", "2016"],
      crosshair: true
    },
    yAxis: {
      min: 0,
      title: {
        text: "Documents"
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat:
        '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
      footerFormat: "</table>",
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [
      {
        data: [
          Records.find({
            active_date: {
              $gte: "2015-01-00T00:00:00.000Z",
              $lte: "2015-12-31T23:59:59.000Z"
            }
          }).count(),
          Records.find({
            active_date: {
              $gte: "2016-01-00T00:00:00.000Z",
              $lte: "2016-12-31T23:59:59.000Z"
            }
          }).count()
        ]
      }
    ]
  };
};
