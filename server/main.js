import { Meteor } from 'meteor/meteor';
import { Records } from '../import/api/Records.js';

Meteor.startup(() => {
  //Remove all documents
  Records.remove({});

  //Init to random active status
  var activeStatusChar = "yn";

  // function to generate random Date :parameter : year (int)
  function randomDate(year){
      var startDate = new Date(year,0,1).getTime();
      var endDate =  new Date(year,12,1).getTime();
      var spaces = (endDate - startDate);
      var timestamp = Math.round(Math.random() * spaces);
      timestamp += startDate;
      return new Date(timestamp);
  }

  // function to expiry Date :parameter : date (date) ,days (int)
  function getExpiryDate(date,days) {
      return setDate(date + parseInt(days));
  }

   // function to add expiry Date :parameter : date (date) ,days (int)
  Date.prototype.addDays = function(days) {
      var date = new Date(this.valueOf());
      date.setDate(date.getDate() + days);
      return date;
  };

  //for loop to insert records for year 2015
  for (var i = 0; i<40; i++){
    date = randomDate(2015);
    expiryDate = date.addDays(5);

    Records.insert({
        active: activeStatusChar.charAt(Math.floor(Math.random() * activeStatusChar.length)) ,
        active_date: date.toISOString() ,
        expiry_date: expiryDate.toISOString()
    });
    
  }

  //for loop to insert records for year 2016
  for (var i = 0; i<10; i++){
    date = randomDate(2016);
    expiryDate = date.addDays(5);

    Records.insert({
        active: activeStatusChar.charAt(Math.floor(Math.random() * activeStatusChar.length)) ,
        active_date: date.toISOString() ,
        expiry_date: expiryDate.toISOString()
    });
    
  }

});
